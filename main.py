from asyncio import constants
from sys import stderr
import time
import const.general as general
import const.messages as messages
import curses
from utils.menu import *

class Game(object):
    def __init__(self, stdscreen):
        self.screen = stdscreen
        curses.curs_set(0)
        
        main_menu_items = [
            ("Play Game", curses.beep)
        ]
        main_menu = Menu(main_menu_items, self.screen)
        main_menu.display()

if __name__ == "__main__":
    curses.wrapper(Game)